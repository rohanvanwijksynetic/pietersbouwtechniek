<?php

$aliases['test'] = array(
  'uri' => 'pietersbouwtechniek.test.synetic.nl',
  'root' => '/var/www/vhosts/pietersbouwtechniek.test.synetic.nl/html',
  'remote-host' => 'localhost',
  'remote-user' => 'jenkins',
);

$aliases['acc'] = array(
  'uri' => 'pietersbouwtechniek.acceptatie.synetic.nl',
  'root' => '/home/pietersbouwtechniek/domains/pietersbouwtechniek/public_html',
  'remote-host' => 'localhost',
  'remote-user' => 'pietersbouwtechniek',
  'path-aliases' => array(
    '%dump-dir' => '/tmp',
  ),
);

$aliases['prod'] = array(
  'uri' => 'pietersbouwtechniek.productie.synetic.nl',
  'root' => '/home/pietersbouwtechniek/domains/pietersbouwtechniek/public_html',
  'remote-host' => 'localhost',
  'remote-user' => 'pietersbouwtechniek',
  'path-aliases' => array(
    '%dump-dir' => '/tmp',
  ),
);
