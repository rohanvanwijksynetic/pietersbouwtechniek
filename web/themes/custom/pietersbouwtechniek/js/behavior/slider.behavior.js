/**
 * @file Implementation of slickslider.
 *
 */

(function ($, Drupal) {
  Drupal.behaviors.slickSlider = {
    attach: function (context) {

      $(".js-slickSlider", context).once("slickSlider").each(function () {


        $(".js-slickSlider").slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true,
          fade: true,
          autoplay: true,
          asNavFor: '.js-slideNav',
          lazyLoad: 'ondemand'
        });

        console.log(9+10);

        $(".js-slideNav").slick({
          asNavFor: '.js-slickSlider',
          slidesToShow: 4,
          slidesToScroll: 1,
          focusOnSelect: true,
          arrows: false,
          lazyLoad: 'ondemand'
        });

      });

    }
  };

})(jQuery, Drupal);