#!/bin/bash

if [ ! -d /app/config/develop/nginx ] || [ ! -d /etc/nginx/conf.d ]; then
    echo 'config dir(s) missing, aborting nginx-additions.'
    exit
fi;

cd /app/config/develop/nginx

# config files
nginx_config_list=( 'nginx-docs.conf' 'nginx-styleguide.conf' 'nginx-simplesamlphp.conf' )


for file in "${nginx_config_list[@]}"
do
    # Add files to nginx config with var substitution.
    if [ -f $file ]; then
        envsubst '$$LANDO_WEBROOT $$LANDO_PROJ_NAME $$LANDO_CUSTOM_THEME_NAME' < $file > /etc/nginx/conf.d/$file
    fi;
done
