# Documentation server.

<VirtualHost *:80>
  ServerName docs.${LANDO_PROJ_NAME}.localhost
  ServerAdmin webmaster@localhost
  DocumentRoot ${LANDO_WEBROOT}/../documentation

  <Directory />
    Options Indexes FollowSymLinks MultiViews
    AllowOverride All
    Order allow,deny
    Allow from all
    Require all granted
  </Directory>

  ErrorLog ${APACHE_LOG_DIR}/error.log
  CustomLog ${APACHE_LOG_DIR}/access.log combined

  SetEnvIf x-forwarded-proto https HTTPS=on

</VirtualHost>

<IfModule mod_ssl.c>
  <VirtualHost *:443>
    ServerName docs.${LANDO_PROJ_NAME}.localhost
    ServerAdmin webmaster@localhost
    DocumentRoot ${LANDO_WEBROOT}/../documentation
    <Directory />
      Options Indexes FollowSymLinks MultiViews
      AllowOverride All
      Order allow,deny
      Allow from all
      Require all granted
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    #   SSL Engine Switch:
    #   Enable/Disable SSL for this virtual host.
    SSLEngine on

    #   A self-signed (snakeoil) certificate can be created by installing
    #   the ssl-cert package. See
    #   /usr/share/doc/apache2/README.Debian.gz for more info.
    #   If both key and certificate are stored in the same file, only the
    #   SSLCertificateFile directive is needed.
    SSLCertificateFile "/certs/cert.crt"
    SSLCertificateKeyFile "/certs/cert.key"

    <FilesMatch "\.(cgi|shtml|phtml|php)$">
        SSLOptions +StdEnvVars
    </FilesMatch>
    <Directory /usr/lib/cgi-bin>
        SSLOptions +StdEnvVars
    </Directory>

    BrowserMatch "MSIE [2-6]" \
        nokeepalive ssl-unclean-shutdown \
        downgrade-1.0 force-response-1.0
    # MSIE 7 and newer should be able to use keepalive
    BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown

    # Pass some common ENVs, its ok if this fails
    SetEnvIf x-forwarded-proto https HTTPS=on

  </VirtualHost>
</IfModule>
