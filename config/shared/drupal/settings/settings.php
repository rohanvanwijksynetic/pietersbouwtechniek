<?php

/**
 * @file
 * Shared Drupal settings.
 *
 * @see sites/[site]/settings.php
 */

/**
 * The default list of directories that will be ignored by Drupal's file API.
 *
 * By default ignore node_modules and bower_components folders to avoid issues
 * with common frontend tools and recursive scanning of directories looking for
 * extensions.
 *
 * @see file_scan_directory()
 * @see \Drupal\Core\Extension\ExtensionDiscovery::scanDirectory()
 */
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

/**
 * The default number of entities to update in a batch process.
 *
 * This is used by update and post-update functions that need to go through and
 * change all the entities on a site, so it is useful to increase this number
 * if your hosting configuration (i.e. RAM allocation, CPU speed) allows for a
 * larger number of entities to be processed in a single batch run.
 */
$settings['entity_update_batch_size'] = 50;

/**
 * Define the used configuration paths and enable the correct config split set.
 */
$config_directories['sync'] = $app_root . '/../config/shared/drupal/sync';
$config['config_split.config_split.' . getenv('SYNETIC_DTAP_ENV')]['status'] = TRUE;

/**
 * Define the path to the current environment configuration.
 */
$environment_config_path = $app_root . '/../config/' . getenv('SYNETIC_DTAP_ENV');

/**
 * Load services definition file.
 */
if (file_exists($environment_config_path . '/drupal/services/services.yml')) {
  $settings['container_yamls'][] = $environment_config_path . '/drupal/services/services.yml';
}
if (file_exists($environment_config_path . '/drupal/services/services.local.yml')) {
  $settings['container_yamls'][] = $environment_config_path . '/drupal/services/services.local.yml';
}

/**
 * File paths.
 */
$settings['file_public_path'] = 'sites/default/files';
$settings['file_private_path'] = '../files/private';
$config['system.file']['path.temporary'] = '../files/tmp';

/**
 * Load environment specific settings.
 */
if (file_exists($environment_config_path . '/drupal/settings/settings.php')) {
  include $environment_config_path . '/drupal/settings/settings.php';
}
if (file_exists($environment_config_path . '/drupal/settings/settings.local.php')) {
  include $environment_config_path . '/drupal/settings/settings.local.php';
}
