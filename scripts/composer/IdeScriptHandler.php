<?php

namespace DrupalProject\composer;

use Composer\Composer;
use Composer\Package\PackageInterface;
use Composer\Script\Event;

/**
 * Class IdeScriptHandler.
 *
 * @package DrupalProject\composer
 */
class IdeScriptHandler {

  /**
   * Attempts to save the vcs mappings.
   *
   * @param \Composer\Script\Event $event
   *   Composer event.
   */
  public static function preparePhpStormIntegration(Event $event) {
    $composer = $event->getComposer();
    $composerConfig = $composer->getConfig();
    $baseDir = dirname($composerConfig->get('vendor-dir'));
    if (!self::validatePhpStormDir($baseDir)) {
      return;
    }
    $xml = '<?xml version="1.0" encoding="UTF-8"?>
              <project version="4">
               <component name="VcsDirectoryMappings"></component>
              </project>';
    $simpleXml = new \SimpleXMLElement($xml);

    $component = $simpleXml->component;

    if (empty($component)) {
      return;
    }
    self::addPhpStormVcsMapping($component, '$PROJECT_DIR$');

    $paths = self::getPreferredInstalledPackagePaths($composer, $baseDir);
    if (empty($paths)) {
      return;
    }
    foreach ($paths as $path) {
      self::addPhpStormVcsMapping($component, $path);
    }

    $simpleXml->saveXML($baseDir . '/.idea/vcs.xml');
  }

  /**
   * Helper function to add vcs mappings.
   *
   * @param \SimpleXMLElement $component
   *   The component element.
   * @param string $directory
   *   The directory to add.
   *
   * @return \SimpleXMLElement
   *   The component element with added attributes.
   */
  protected static function addPhpStormVcsMapping(\SimpleXMLElement $component, $directory) {
    $mapping = $component->addChild('mapping');
    $mapping->addAttribute('directory', $directory);
    $mapping->addAttribute('vcs', 'Git');

    return $mapping;
  }

  /**
   * Whether the .idea directory exists.
   *
   * @param string $baseDir
   *   The baseDir.
   *
   * @return bool
   *   If it exists or not.
   */
  protected static function validatePhpStormDir($baseDir) {
    $ideaDir = $baseDir . '/.idea';
    return is_dir($ideaDir);
  }

  /**
   * Get the package directories.
   *
   * @param Composer $composer
   *   Composer instance.
   * @param string $baseDir
   *   The base directory.
   * @param string $searchString
   *   The search string.
   *
   * @return array
   *   The found paths.
   */
  protected static function searchPackagePaths(Composer $composer, $baseDir, $searchString) {
    $localRepository = $composer->getRepositoryManager()->getLocalRepository();
    $matches = $localRepository->search($searchString);

    if (empty($matches)) {
      return [];
    }

    $paths = [[]];
    foreach ($matches as $match) {
      $paths[] = self::getPackagePaths($composer, $baseDir, $match['name']);
    }

    return array_merge(...$paths);
  }

  /**
   * Get the paths for a package (including dependencies).
   *
   * @param Composer $composer
   *   Composer instance.
   * @param string $baseDir
   *   The base directory.
   * @param string $packageName
   *   The packageName.
   *
   * @return array
   *   The paths for the package.
   */
  protected static function getPackagePaths(Composer $composer, $baseDir, $packageName) {
    $paths = [];
    $localRepository = $composer->getRepositoryManager()->getLocalRepository();
    $packages = $localRepository->findPackages($packageName);

    if (empty($packages)) {
      return [];
    }

    foreach ($packages as $package) {
      $paths[$package->getName()] = self::getPackagePath($composer, $baseDir, $package);
    }

    return $paths;
  }

  /**
   * Get the package path.
   *
   * @param Composer $composer
   *   Composer instance.
   * @param string $baseDir
   *   The base directory.
   * @param \Composer\Package\PackageInterface $package
   *   The package.
   *
   * @return string
   *   The package path.
   */
  protected static function getPackagePath(Composer $composer, $baseDir, PackageInterface $package) {
    $installationManager = $composer->getInstallationManager();
    $path = $installationManager->getInstallPath($package);
    if (strpos($path, $baseDir) === 0) {
      $path = str_replace($baseDir . '/', '', $path);
    }

    return '$PROJECT_DIR$/' . $path;
  }

  /**
   * Get the preferred package path.
   *
   * @param Composer $composer
   *   Composer instance.
   * @param string $baseDir
   *   The base directory.
   *
   * @return array
   *   The preferred install paths.
   */
  protected static function getPreferredInstalledPackagePaths(Composer $composer, $baseDir) {
    $composerConfig = $composer->getConfig();
    $preferredInstall = $composerConfig->get('preferred-install');
    $paths = [[]];

    if (empty($preferredInstall) || !is_array($preferredInstall)) {
      return [];
    }

    foreach ($preferredInstall as $packageKey => $preferredInstallSetting) {
      if ($preferredInstallSetting !== 'source') {
        continue;
      }
      if ($packageKey === '*') {
        continue;
      }
      if (strpos($packageKey, '*') !== FALSE) {
        $paths[] = self::searchPackagePaths($composer, $baseDir, $packageKey);
        continue;
      }

      $paths[] = self::getPackagePaths($composer, $baseDir, $packageKey);
    }

    return array_merge(...$paths);
  }

}
