<?php

namespace DrupalProject\composer;

use Composer\Script\Event;
use DrupalFinder\DrupalFinder;
use Symfony\Component\Filesystem\Filesystem;
use Webmozart\PathUtil\Path;

/**
 * Class ScriptHandler.
 *
 * @package DrupalProject\composer
 */
class ScriptHandler {

  /**
   * List of environments to maintain settings for.
   *
   * @var array
   */
  protected static $environments = [
    'shared',
    'develop',
    'test',
    'acceptation',
    'production',
  ];

  /**
   * List of environment directories.
   *
   * @var array
   */
  protected static $environmentDirs = [
    'drupal/sync',
    'drupal/services',
    'drupal/settings',
  ];

  /**
   * List of drupal directories.
   *
   * @var array
   */
  protected static $drupalDirs = [
    'web/modules/contrib',
    'web/modules/custom',
    'web/modules/synetic',
    'web/profiles/contrib',
    'web/profiles/synetic',
    'web/themes/contrib',
    'web/themes/custom',
    'web/libraries',
  ];

  /**
   * List of file directories.
   *
   * @var array
   */
  protected static $filesDirs = [
    'files/public',
    'files/private',
  ];

  /**
   * List of solr directories.
   *
   * @var array
   */
  protected static $solrDirs = [
    'config/shared/solr',
  ];

  /**
   * List of drush directories.
   *
   * @var array
   */
  protected static $drushDirs = [
    'drush/alias',
  ];

  /**
   * List of files to rewrite for placeholders.
   *
   * @var array
   */
  protected static $rewriteFiles = [
    'drush/alias/PROJECT_NAME.aliases.drushrc.php',
    'documentation/_coverpage.md',
    'documentation/index.html',
    '.lando.yml',
  ];

  /**
   * List of files to rename for placeholders.
   *
   * @var array
   */
  protected static $renameFiles = [
    'drush/alias/PROJECT_NAME.aliases.drushrc.php',
  ];

  /**
   * List of unnecessary files that should be removed.
   *
   * These files shouldn't be publicly accessible.
   *
   * @var array
   */
  protected static $unnecessaryFiles = [
    'INSTALL.txt',
    'sites/example.sites.php',
    'sites/example.settings.local.php',
    'sites/development.services.yml',
    'core/CHANGELOG.txt',
    'core/COPYRIGHT.txt',
    'core/INSTALL.mysql.txt',
    'core/INSTALL.pgsql.txt',
    'core/INSTALL.sqlite.txt',
    'core/INSTALL.txt',
    'core/LICENSE.txt',
    'core/MAINTAINERS.txt',
    'core/UPDATE.txt',
  ];

  /**
   * Path of the configuration files.
   *
   * @var string
   */
  protected static $configPath = 'config';

  /**
   * Mapping for project name to database port.
   *
   * @var array
   */
  protected static $databasePortMapping = [
    'a' => 2,
    'b' => 2,
    'c' => 2,
    'd' => 3,
    'e' => 3,
    'f' => 3,
    'g' => 4,
    'h' => 4,
    'i' => 4,
    'j' => 5,
    'k' => 5,
    'l' => 5,
    'm' => 6,
    'n' => 6,
    'o' => 6,
    'p' => 7,
    'q' => 7,
    'r' => 7,
    's' => 7,
    't' => 8,
    'u' => 8,
    'v' => 8,
    'w' => 9,
    'x' => 9,
    'y' => 9,
    'z' => 9,
  ];

  /**
   * Create the required files for a drupal installation.
   *
   * @param Event $event
   *   Composer script event.
   *
   * @throws \Exception
   */
  public static function createRequiredFiles(Event $event) {
    $filesystem = new Filesystem();
    $root = self::getProjectRoot($event);
    $drupalRoot = self::getDrupalRoot($root);

    self::createDirectories($filesystem, $root, self::$drupalDirs);
    self::createDirectories($filesystem, $root, self::$filesDirs);
    self::createDirectories($filesystem, $root, self::$solrDirs);
    self::createDirectories($filesystem, $root, self::$drushDirs);
    self::createDirectoriesForEnvironments($filesystem, $root, self::$environmentDirs);

    self::createSettingsPhpForEnvironments($filesystem, $root, $drupalRoot, $event);
    self::createFilesDirectoryForDrupal($filesystem, $drupalRoot, $event);

    self::processPlaceholders($filesystem, $root);

  }

  /**
   * Cleanup unnecessary Drupal files after a composer install/update.
   *
   * @param \Composer\Script\Event $event
   *   The composer script event.
   */
  public static function cleanupUnnecessaryFiles(Event $event) {
    $filesystem = new Filesystem();
    $drupalRoot = self::getDrupalRoot(self::getProjectRoot($event));

    foreach (self::$unnecessaryFiles as $file) {
      self::removeFile($filesystem, $drupalRoot, $file);
    }
  }

  /**
   * Create the directories based on a mapped array().
   *
   * @param \Symfony\Component\Filesystem\Filesystem $filesystem
   *   Composer FileSystem.
   * @param string $root
   *   The project root.
   * @param array $directories
   *   The directories to create.
   */
  protected static function createDirectories(Filesystem $filesystem, $root, $directories) {
    foreach ($directories as $directory) {
      self::createDirectory($filesystem, $root, $directory);
    }
  }

  /**
   * Create a single directory.
   *
   * @param \Symfony\Component\Filesystem\Filesystem $filesystem
   *   Composer FileSystem.
   * @param string $root
   *   The project root.
   * @param string $directory
   *   The directory to create.
   */
  protected static function createDirectory(Filesystem $filesystem, $root, $directory) {
    if ($filesystem->exists($root . '/' . $directory)) {
      return;
    }
    $filesystem->mkdir($root . '/' . $directory);
    $filesystem->touch($root . '/' . $directory . '/.gitkeep');
  }

  /**
   * Create directories for each environment.
   *
   * @param \Symfony\Component\Filesystem\Filesystem $filesystem
   *   Composer FileSystem.
   * @param string $root
   *   The project root.
   * @param array $directories
   *   The directories to create.
   */
  protected static function createDirectoriesForEnvironments(Filesystem $filesystem, $root, $directories) {
    foreach (self::$environments as $environment) {
      $environment_root = $root . '/' . self::$configPath . '/' . $environment;
      self::createDirectories($filesystem, $environment_root, $directories);
    }
  }

  /**
   * Create settings php for the environments.
   *
   * @param \Symfony\Component\Filesystem\Filesystem $filesystem
   *   Composer FileSystem.
   * @param string $root
   *   The project root.
   * @param string $drupalRoot
   *   The Drupal root.
   * @param \Composer\Script\Event $event
   *   The composer event.
   *
   * @throws \Exception
   */
  protected static function createSettingsPhpForEnvironments(Filesystem $filesystem, $root, $drupalRoot, Event $event) {
    foreach (self::$environments as $environment) {
      $settings_path = $root . '/' . self::$configPath . '/' . $environment . '/drupal/settings/settings.php';

      if ($filesystem->exists($settings_path) || !$filesystem->exists($drupalRoot . '/sites/default/default.settings.php')) {
        continue;
      }

      $filesystem->copy($drupalRoot . '/sites/default/default.settings.php', $settings_path);
      require_once $drupalRoot . '/core/includes/bootstrap.inc';
      require_once $drupalRoot . '/core/includes/install.inc';
      $settings['config_directories'] = [
        CONFIG_SYNC_DIRECTORY => (object) [
          'value' => Path::makeRelative($root . '/' . self::$configPath . '/' . $environment . '/drupal/sync/', $drupalRoot),
          'required' => TRUE,
        ],
      ];
      drupal_rewrite_settings($settings, $settings_path);
      $filesystem->chmod($settings_path, 0666);
      $event->getIO()->write('Create a settings.php file with chmod 0666 for ' . $environment);
    }
  }

  /**
   * Create a files directory within drupal.
   *
   * @param \Symfony\Component\Filesystem\Filesystem $filesystem
   *   Composer FileSystem.
   * @param string $drupalRoot
   *   The Drupal root.
   * @param \Composer\Script\Event $event
   *   The composer event.
   */
  protected static function createFilesDirectoryForDrupal(Filesystem $filesystem, $drupalRoot, Event $event) {
    if ($filesystem->exists($drupalRoot . '/sites/default/files')) {
      return;
    }

    $oldmask = umask(0);
    $filesystem->mkdir($drupalRoot . '/sites/default/files', 0777);
    umask($oldmask);
    $event->getIO()->write('Create a sites/default/files directory with chmod 0777');
  }

  /**
   * Process the placeholders used in file content and file names.
   *
   * @param \Symfony\Component\Filesystem\Filesystem $filesystem
   *   Composer FileSystem.
   * @param string $root
   *   The project root.
   */
  protected static function processPlaceholders(Filesystem $filesystem, $root) {
    $project_name = basename($root);
    self::rewriteFiles($root, $project_name);
    self::renameFiles($filesystem, $root, $project_name);
  }

  /**
   * Process placeholders for file contents based on file list on this class.
   *
   * @param string $root
   *   The project root.
   * @param string $project_name
   *   The project name.
   */
  protected static function rewriteFiles($root, $project_name) {
    $files = self::$rewriteFiles;
    foreach ($files as $file) {
      $file_path = $root . '/' . $file;
      if (!file_exists($file_path)) {
        continue;
      }

      $contents = file_get_contents($file_path);
      $contents = str_replace(
        [
          'PROJECT_NAME',
          'PROJECT_DATABASE_PORT',
        ], [
          $project_name,
          self::generateDatabasePort($project_name),
        ],
        $contents
      );
      file_put_contents($file_path, $contents);
    }
  }

  /**
   * Remove a file from the filesystem.
   *
   * @param \Symfony\Component\Filesystem\Filesystem $filesystem
   *   The file system.
   * @param string $root
   *   The root directory.
   * @param string $file_name
   *   The filename.
   */
  protected static function removeFile(Filesystem $filesystem, $root, $file_name) {
    if (!$filesystem->exists($root . '/' . $file_name)) {
      return;
    }
    $filesystem->remove($root . '/' . $file_name);
  }

  /**
   * Process placeholders for file names based on file list on this class.
   *
   * @param \Symfony\Component\Filesystem\Filesystem $filesystem
   *   Composer FileSystem.
   * @param string $root
   *   The project root.
   * @param string $project_name
   *   The project name.
   */
  protected static function renameFiles(Filesystem $filesystem, $root, $project_name) {
    $files = self::$renameFiles;
    foreach ($files as $file) {
      $file_path = $root . '/' . $file;
      if (!file_exists($file_path)) {
        continue;
      }
      $new_file = str_replace('PROJECT_NAME', $project_name, $file);
      $new_file_path = $root . '/' . $new_file;
      $filesystem->rename($file_path, $new_file_path);
    }
  }

  /**
   * Generate database port from the project name.
   *
   * @param string $project_name
   *   The project name.
   *
   * @return string
   *   A 5 digit port number.
   */
  protected static function generateDatabasePort($project_name) {
    $port = '';
    for ($i = 0; $i <= 3; $i++) {
      if (empty(self::$databasePortMapping[$project_name[$i]])) {
        continue;
      }

      $port .= self::$databasePortMapping[$project_name[$i]];
    }

    return str_pad($port, 5, '3', STR_PAD_LEFT);
  }

  /**
   * Get the project root from the composer event based on the vendor dir.
   *
   * @param \Composer\Script\Event $event
   *   The composer script event.
   *
   * @return string
   *   The project root based on the vendor dir.
   */
  protected static function getProjectRoot(Event $event) {
    return \dirname($event->getComposer()->getConfig()->get('vendor-dir'));
  }

  /**
   * Get the Drupal root directory from the project root.
   *
   * @param string $root
   *   The project root.
   *
   * @return string
   *   The Drupal root directory.
   */
  protected static function getDrupalRoot($root) {
    $drupalFinder = new DrupalFinder();
    $drupalFinder->locateRoot($root);
    return $drupalFinder->getDrupalRoot();
  }

}
