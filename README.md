# Pieters Bouw Techniek
A small project by Rohan for **Synetic front-end Academy Course**. This project uses [pietersbouwtechniek website](https://pietersbouwtechniek.nl/) as an inspiration.

---

### What you need for this project
* Docker
* Lando
* NodeJS
* Composer

### Get started with this project
1. Clone this repo: `git clone git@bitbucket.org:rohanvanwijksynetic/pietersbouwtechniek.git` via [SSH (Secure Shell)](https://nl.wikipedia.org/wiki/Secure_Shell)
2. `cd pietersbouwtechniek`
2. Run `composer install`
3. Run `lando start`

![Drupal8](https://www.drupal.org/files/drupal%208%20logo%20Stacked%20CMYK%20300.png)
